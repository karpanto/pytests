#!/usr/bin/env python
 
import os
import re
 
# patters of files to be found
patterns = [line.strip() for line in open('patterns.txt')] 

def make_abs_path(path, file):
    path = os.path.join(dirpath, file)
    abs_path = os.path.abspath(path)
    return abs_path
 
def file_match(file):
    for p in patterns:
        if re.search(p, file):
            return True
    return False
 
for dirpath, dirnames, files in os.walk("./"):
    for file in files:
        path = make_abs_path(dirpath, file)
        if file_match(file):
            print(path)
